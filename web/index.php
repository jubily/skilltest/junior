<?php
/**
 * Created by PhpStorm.
 * User: Sylvain Glaçon
 * Date: 21/03/2018
 * Time: 11:46
 */

$controller = "index";

if (isset($_GET["page"])) {

    $controller = $_GET["page"];

}

$file = "{$controller}.controller.php";

require("../src/{$file}");
