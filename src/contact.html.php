<!DOCTYPE html>
<html>
    <?php require("header.html.php") ?>
    <body>
        <h1>
            Vous avez aimé notre sélecteur d'arrière-plan ?<br/>
            Contactez-nous pour encore plus de surprises !
        </h1>
        <form method="post">
            <?php if($confirmation) { ?>
                <p>Votre message a bien été envoyé ! Nous vous répondrons très vite !</p>
            <?php } ?>
            <label>Votre adresse email</label>
            <input type="text" name="email" />
            <label>Votre message</label>
            <textarea name="message" rows="10"></textarea>
            <button>Envoyer !</button>
        </form>
    </body>
</html>
