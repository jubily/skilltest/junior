<?php
/**
 * Created by PhpStorm.
 * User: Sylvain Glaçon
 * Date: 21/03/2018
 * Time: 11:58
 */

return [
    "hw1_bixby_creek_bridge" => "HW1 - Bixby Creek Bridge",
    "la_panorama" => "Los Angeles - Panorama",
    "sd_cityscape" => "San Diego - Panorama",
    "sd_gaslamp_quarter" => "San Diego - Quartier Gaslamp",
    "sf_bay_bridge" => "San Francisco - Bay Bridge",
    "sf_downtown" => "San Francisco - Centre-ville",
    "sf_golden_gate" => "San Francisco - Golden Gate",
    "sf_lombard_street" => "San Francisco - Lombard Street",
    "sf_painted_ladies" => "San Francisco - Painted ladies",
    "sf_palace_of_fine_arts" => "San Francisco - Palais des beaux-arts",
    "sf_vista_point" => "San Francisco - Vista Point",
    "yos_el_capitan" => "Yosemite Valley - El Capitàn"
];
