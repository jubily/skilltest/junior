<?php
/**
 * Created by PhpStorm.
 * User: Sylvain Glaçon
 * Date: 21/03/2018
 * Time: 14:59
 */

$confirmation = false;
if (count($_POST) > 1) {

    $email = $_POST["email"];
    $message = $_POST["message"];

    @mail("contact@jubily.fake", "Contact sur le site", $message, implode("\r\n", [
        "From: {$email}",
        "ReplyTo: {$email}"
    ]));

    $confirmation = true;

}

require ("contact.html.php");
