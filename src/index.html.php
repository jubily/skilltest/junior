<!DOCTYPE html>
<html>
    <?php require("header.html.php") ?>
    <body style="background-image:url('img/<?= $image ?>')">
        <form method="get">
            <select name="back" id="background-selector">
                <?php foreach ($data as $filename => $title){ ?>
                    <option value="<?= $filename ?>.jpg" <?= ($activeImage == $filename) ? "selected" : ""; ?>>
                        <?= $title ?>
                    </option>
                <?php } ?>
            </select>
            <p><a href="?page=contact">Contactez-nous</a></p>
        </form>
    </body>
</html>
