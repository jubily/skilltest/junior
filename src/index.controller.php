<?php
/**
 * Created by PhpStorm.
 * User: Sylvain Glaçon
 * Date: 21/03/2018
 * Time: 11:55
 */

$data = require ("data.php");

if (isset($_GET["back"])) {

    $image = $_GET["back"];

} else {

    $images = scandir("img");
    $image = $images[array_rand($images)];

}

$activeImage = explode(".", $image)[0];

require ("index.html.php");
