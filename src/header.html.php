<head>
    <meta charset="UTF-8" />
    <title><?= (isset($page_title) ? $page_title : "Jubily background switcher") ?></title>
    <link rel="stylesheet" href="<?= $controller ?>.style.css" />
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script>
        $(function(){

            document.getElementById("background-selector").onchange = function(){

                this.parentNode.submit();

            };

        });
    </script>
</head>
