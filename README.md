# Bienvenue chez Jubily !

Afin de préparer ou compléter notre entretien, nous vous proposons un test d'**une heure environ**, qui nous permettra 
de juger de votre expérience et de vos connaissances dans la programmation pour le Web, dans tous ses aspects.

**Vous avez impérativement besoin pour ce test d'un éditeur de code, capable de récupérer un projet *Git***. Aucun autre 
outil n'est *obligatoire*, mais plus vous en aurez à nous présenter, meilleure sera notre appréhension globale de vos 
qualités techniques.

Le projet que vous venez de télécharger depuis *Gitlab* contient déjà un certain nombre de fichiers ; il est fonctionnel
lorsqu'il est configuré sur un serveur *Apache*, mais n'est pas exempt de problèmes : son architecture n'est pas adaptée 
à une utilisation en production et quelques bugs persistent. Il vous appartient, en vous aidant des consignes 
ci-dessous, d'améliorer ce projet.

Prenez soin de lire **tout l'énoncé** : les consignes et questions posées sont ordonnées par ordre de priorité, vous 
avez une heure pour en traiter **le plus possible, mais pas obligatoirement dans l'ordre**. Gérez votre temps au mieux. 
Vous présenterez ensuite votre travail à notre *lead developer* et aux deux co-fondateurs de Jubily.

## Consignes

- Dans le fichier `web/index.php`, un bug peut éventuellement entraîner une erreur fatale. Trouvez-le, et corrigez-le.
- Lorsque l'utilisateur n'a pas sélectionné d'images, le code dans `src/index.controller.php` sélectionne une image au 
hasard. Seulement, un bug se produit parfois et l'arrière-plan reste blanc. Trouvez-le, et corrigez-le.
- En modifiant le fichier `web/index.style.css`, faites-en sorte :
    - que la taille de l'arrière-plan s'adapte automatiquement à l'écran sur lequel il est affiché (en hauteur **ou** en 
    largeur) ;
    - que le centre de l'image en arrière-plan corresponde toujours au centre de l'écran.
- Le directeur général demande qu'il n'y ait plus besoin de recharger la page lorsque l'utilisateur change 
l'arrière-plan. Modifiez le code (`src/header.html.php`) en conséquence.
- Le sélecteur d'images ne fonctionne correctement qu'avec les fichiers `.jpg`. Modifiez le code partout où vous le 
jugez nécessaire pour qu'il fonctionne avec tous les fichiers "image" que vous connaissez.
- Le code chargé de traiter les données reçues du formulaire de contact (`web/contact.controller.php`) présente un gros 
défaut de conception. Trouvez-le et corrigez-le du mieux que vous le pouvez.
- Modifiez les CSS et leur intégration pour éviter toute duplication de code entre les fichiers `index.style.css` et 
`contact.style.css`.
- Dans un mail, le nombre de caractères dans chaque ligne du message ne doit pas excéder 70, et le caractère de retour à 
la ligne doit être un CRLF (`\r\n`). Comment pouvez-vous vous en assurer avant d'envoyer le mail ?

## Questions à préparer

- Comment amélioreriez-vous la maintenance et la mise à jour du code HTML dans ce projet ?
- Quel(s) problème(s) peut poser l'intégration de jQuery telle qu'elle est faite ?

## Quelques questions *annexes*

- Quel est le rôle du fichier `.htaccess` dans le dossier `web/` ?

## Bonus

- Effectuer un (ou plusieurs) *commit* de votre travail sur notre dépôt *Gitlab* ;
- Configurer un serveur Apache sur votre machine (si vous n'en avez pas) ;
- Configurer votre serveur Apache local (si vous en avez un) pour faire fonctionner le projet ;

Il nous sera plus aisé de juger de vos qualités techniques si vous effectuez ces actions, mais elles ne sont pas 
essentielles.  

## Standard disclaimer

N'ayez pas peur, le projet sur lequel vous allez travailler ne reflète ni l'état passé, ni l'état actuel, ni l'état 
futur de la plateforme Jubily :)
